package com.maBanque;

public class CompteImpl implements Compte{ 
 
 	float solde; 
 	 
 	@Override 
 	public void crediter(float credit) throws Exception {
 		this.solde = solde + credit;
 	}

	@Override
	public float getSolde() {
		return this.solde;
	}

	@Override
	public float debiter(float debit) throws Exception {
		this.solde = solde - debit;
		if(this.solde > debit) {
			return debit;
		} else if (this.solde < debit){
			return solde;
		} else if (debit < 20 || debit > 1000) {
			throw new Exception();
		} else throw new Exception();
	}

	@Override
	public void setSolde(float solde) throws Exception {
		if(solde < 0) throw new Exception();
		this.solde = solde;
	} 
  
} 
